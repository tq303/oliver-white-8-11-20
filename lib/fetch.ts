import fetch from 'isomorphic-unfetch'

export default async function fetcher(
    input: RequestInfo,
    init?: RequestInit
): Promise<any> {
    try {
        const res = await fetch(input, init)

        if (!res.ok) {
            const error = await res.text()
            throw error
        } else {
            return res.json()
        }
    } catch (e) {
        throw e
    }
}