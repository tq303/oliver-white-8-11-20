const path = require('path')
const multer = require('multer')

const permittedMimeTypes = [
    {
        extension: 'jpg',
        mimetype: 'image/jpeg',
    },
    {
        extension: 'png',
        mimetype: 'image/png',
    },
]

const storage = multer.diskStorage({
    destination(req, file, cb) {
        cb(null, path.resolve(__dirname, '../files'))
    },
    filename(req, file, cb) {
        const fileType = permittedMimeTypes.find(f => f.mimetype === file.mimetype)

        const random = Math.round(Math.random() * 1E9)

        cb(null, `${random}.${fileType.extension}`)
    }
})

module.exports = multer({
    storage,
    limits: {
        fileSize: 1024 * 1024 * 10
    },
    fileFilter (req, file, cb) {
        const fileType = permittedMimeTypes.find(f => f.mimetype === file.mimetype)

        if (fileType) {
            cb(null, true)
        } else {
            cb(new Error(`mime-type : ${file.mimetype} : not permitted`))
        }
    }
})
