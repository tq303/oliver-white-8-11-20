const fs = require('fs')
const path = require('path')
const express = require('express')
const router = express.Router()

const upload = require('./upload')
const fileUpload = upload.single('file')

const {
    cacheFileDetails,
    filteredFiles,
    deleteFile,
} = require('./files.helper')

router.get('/files', (req, res) => {
    res.send(filteredFiles(req.query.search))
})

router.post('/files', (req, res) => {
    fileUpload(req, res, err => {
        if (err) {
            res.status(500).send(err.message)
        } else {
            cacheFileDetails()
            res.send('ok')
        }
    })
})

router.delete('/files/:uuid', upload.single('file'), (req, res) => {
    try {
        deleteFile(req.params.uuid)
        res.send('ok')
    } catch(e) {
        res.status(500).send(e.code)
    }

    cacheFileDetails()
})

module.exports = router
