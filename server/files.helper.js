const path = require('path')
const fs = require('fs')

const filesPath = path.resolve(__dirname, '../files')

/* helper functions */
function bytesToKilobytes(bytes) {
    return Math.floor(bytes / 1024)
}

function fileSizeKiloBytes (file) {
    const stats = fs.statSync(path.resolve(filesPath, file))
    
    return bytesToKilobytes(stats.size)
}

function getFileDetails() {
    return JSON.parse(fs.readFileSync(path.resolve(__dirname, 'file-list.json'), 'utf8'))
}

/* export functions */
function cacheFileDetails () {
    const fileTypes = ['jpg', 'jpeg', 'png']

    const files = fs.readdirSync(filesPath)
        .filter(file => fileTypes.includes(file.split('.').pop()))
        .map(file => ({
            uuid: file.split('.').shift(),
            name: file,
            size: fileSizeKiloBytes(file)
        }))
    
    fs.writeFileSync(path.resolve(__dirname, 'file-list.json'), JSON.stringify(files, null, 4))
}

function filteredFiles(query = '') {
    const files = getFileDetails()

    return files.filter(file => {
        const fileName = file.name.toLowerCase()
        const searchTerm = query.toLowerCase()
        return !searchTerm || fileName.includes(searchTerm)
    })
}

function deleteFile(uuid) {
    const files = getFileDetails()

    const file = files.find(file => file.uuid === uuid)

    if (!file) throw new Error('file not found')

    fs.unlinkSync(path.resolve(filesPath, file.name))
}

module.exports = {
    cacheFileDetails,
    filteredFiles,
    deleteFile,
}
