# Oliver White - 8-11-20

## Installation

Prerequisites required:

- nodejs lts
- yarn

To install & start the application in in a `development` environment run the following from the root directory:

```sh
yarn
yarn dev
```

Application is then accessible from `http://localhost:3000`

## Security

List security concerns that have been addressed:

- create random filename
- don't display images in case of metadata exploits
- ensure files aren't public
- content secuirty policy to restrict inline-src and only allow same origin scripts

List of security concerns that have *not* been addressed:

- correctly validate the file type on upload
- csrf exploits

## Improvements

- csrf `express.js` token
- AirBNB linter
- files pagination
- better test config
- better caching of file details
- display the images
- create separate, smaller web version of each image for display
- smoother re-renders of files list
- repair production build

## Libraries

- `express.js`: Used to create custom `next.js` server
- `multer`: Handles the complicated `multipart/form-data` files uploads and file validation (size, mimetype)
- `next.js`: Allows for use of `react`, `typescript` and `express` with integrated _css modules_ out of the box
- `sass`: Allows conversion of `*.sass` files
- `swr`: super useful `react` remote hooks library, used to fetch data
- `jest`: Test library
- `typescript`: Type checked javascript

## API

I created a separate `express.js` server for `next.js` so that I could handle the file upload and all other endpoints better. I am more familiar with `express.js` and it has well supported libraries to make file upload setup quicker.

### GET /api/files

File list

- File list searchable by name
- Returns a files array `Files[]`
- Accepts `query` query string for search

### POST /api/files

Image upload

- Allows image upload and updates `file-list.json`
- Returns text `ok` on success
- Accepts no parameters

### DELETE /api/files/:uuid

Delete image

- Allows image deletion and updates `file-list.json`
- Returns text `ok` on success
- Accepts `uuid` url parameter

## Other notes

All page functionality is located in the root of the `pages` folder as there is only one page required.
Reusable components are located in the `components` folder.
Index page components are located in the `pages/components` folder.
The repo has a `files` directory with both `jpg` and `png` examples.
A `baseUrl` is used for Typescript to locate folders from root level e.g. `lib/*` & `components/*`
File list is cached in `file-list.json` to reduce the amount of cpu heavy file calcuations
There is a `DisableScreen` component to handle modal like pop ups for errors and preloaders

### Tests

Tests can be ran with the following:

```sh
yarn test
```

> Note this only covers component snapshots currently

If snapshots need updating run the following:

```sh
yarn test -u
```

### todos

- [ ] UI
  - [ ] AirBNB linter
  - [x] add `baseUrl` to `tsconfig`
  - [x] flex container cmpt
    - [x] max 960px
    - [x] breakpoint 480px
    - [x] flex order or input and button
  - [x] context api
    - [x] search function
    - [x] delete function
  - [x] file list
    - [x] use css grid layout
    - [x] upload indicator
    - [x] files endpoint response
    - [x] use context
    - [x] props type
    - [x] `swr` request hook
  - [x] card cmpt
    - [x] add `title` text overflow
    - [x] better flex layout
    - [x] :active css
  - [x] button cmpt
    - [x] create component and css module
    - [x] spread props for allow for use of ref
  - [x] upload cmpt
    - [x] jpg & png
    - [x] handle click progamatically
  - [x] file size count cmpt
    - [x] create component and css module
    - [x] use context
    - [x] responsive
  - [x] search input cmpt
    - [x] create component and css module
    - [x] `swr` request hook
    - [x] search debounce
  - [x] Tests
    - [x] jest snapshot test
  - [x] Typescript
  - [x] Sass
    - [x] update next config to allow `styles` folder alias
  - [x] Overall page responsiveness
  - [x] Header section component
  - [x] PageContainer
    - [x] render props
    - [x] prevent page collapsing when searching
  - [x] disable screen cmpt
    - [x] position fix translucent black screen
    - [x] add alert functionality
    - [x] add render props to `message` and button text props
- [ ] Server
  - [x] add custom express server
  - [x] file size function
  - [ ] files `get` endpoint
    - [ ] secure search endpoint
    - [x] save file list calculation to file for efficiency
    - [x] filter files from search query
  - [x] upload `post` endpoint
    - [x] `multer` upload route
    - [x] return & update file list on upload
    - [x] random file name and create uuid
    - [x] create better file name
    - [x] use hash as react key in file list array
    - [x] limit filesize
  - [ ] secure mime types
    - [ ] check files actual mime type
    - [x] upload check
    - [x] `input` tag accept `image/jpg` and `image/png`
    - [x] test mime type upload check works
    - [x] ensure mime types are secure
  - [ ] `delete` endpoint
    - [ ] secure endpoint
    - [x] send uuid and search in body
    - [x] delete file
    - [x] return updated file list
- [ ] Secutiry
  - [ ] ensure files are secure
  - [ ] further secure endpoints
  - [ ] CSRF protection express
  - [ ] correctly validate the file type
  - [x] add CSP to all pages
  - [x] randomly generate filename (reflected xss)
  - [x] Whitelist Extensions
  - [x] don't display images in case of metadata exploits
  - [x] store files outside of the webroot
- [ ] Extras
  - [ ] add `react.memo` to display components
  - [ ] comment code more
  - [ ] use different fetch library/create fetch export with error handling
  - [ ] better caching of files details
  - [ ] files pagination
  - [ ] better test setup
  - [ ] remove test warnings
  - [x] update `fetchFiles` hook to use context
  - [x] update `mutateFiles` hook to use context
  - [x] tidy up export function names
  - [x] test unfound file, on delete, and return relevant status code
  - [x] move endpoint generation into single file/place
  - [x] How to handle swr errors (pass options object)
  - [x] improve fetch error handling
  - [x] decide on error response format (text)
  - [x] remove `delete` and `post` files response and revalidate
