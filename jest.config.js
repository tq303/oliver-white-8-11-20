module.exports = {
    transform: {
        '^.+\\.(js|jsx|ts|tsx)$': '<rootDir>/node_modules/babel-jest',
        '^.+\\.css$': '<rootDir>/jest.transform.css.js',
    },
    transformIgnorePatterns: [
        '/node_modules/',
        '^.+\\.module\\.(css|sass|scss)$',
    ],
    moduleNameMapper: {
        '^components/(.*)': '<rootDir>/components/$1',
        '^lib/(.*)': '<rootDir>/lib/$1',
        '^.+\\.module\\.(css|sass|scss)$': 'identity-obj-proxy',
    }
};