const path = require('path')

module.exports = {
    sassOptions: {
        includePaths: [path.join(__dirname, 'styles')],
    },
    async headers() {
        return [
            {
                source: '/(.*?)',
                headers: [
                    {
                        key: 'Content-Security-Policy',
                        value: "default-src 'self' 'unsafe-eval'; style-src 'self' 'unsafe-inline'; img-src 'self' data:",
                    },
                ],
            },
        ]
    }
}
