import Button from 'components/Button'

import styles from './Card.module.scss'

type Props = {
    uuid: string,
    name: string,
    size: number,
    onDelete: (arg: string) => void
}

export default function Card ({ uuid, name, size, onDelete }: Props) {
    return (
        <div className={styles.card}>
            <h3 className={styles.title}>{name}</h3>
            <div className={styles.flexContainer}>
                <p>{size}kb</p>
                <Button onClick={() => onDelete(uuid)}>delete</Button>
            </div>
        </div>
    )
}