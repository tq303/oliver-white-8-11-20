import styles from './DisableScreen.module.scss'

import Button from 'components/Button'

type Props = {
    message: string | (() => JSX.Element),
    onClose?: () => void,
    onAccept?: (uuid: string) => void | Promise<void>,
    textAccept?: string,
    textClose?: string,
}

export default function DisableScreen ({
    message,
    onClose,
    onAccept,
    textAccept,
    textClose
}: Props) {
    return (
        <div className={styles.container}>
            <div className={styles.alertContainer}>
                {typeof message === 'function' ? message() : <p>{message}</p>}
                <div className={styles.btnContainer}>
                    {onAccept && <Button onClick={onAccept}>{textAccept ? textAccept : 'OK'}</Button>}
                    {onClose && <Button onClick={onClose}>{textClose ? textClose : 'CLOSE'}</Button>}
                </div>
            </div>
        </div>
    )
}
