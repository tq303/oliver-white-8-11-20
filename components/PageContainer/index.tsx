import styles from './PageContainer.module.scss'

type Props = {
    renderHeader: () => JSX.Element
    renderFileList: () => JSX.Element
}

export default function PageContainer ({ renderHeader, renderFileList }: Props) {
    return (
        <div className={styles.container}>
            <div>
                {renderHeader()}
            </div>
            <div>
                {renderFileList()}
            </div>
        </div>
    )
}
