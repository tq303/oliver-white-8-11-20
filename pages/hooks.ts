import { useContext } from 'react'
import useSWR, { mutate, responseInterface } from 'swr'

import fetch from 'lib/fetch'

import Context, { ApiFile } from './context'

function createEndpoint(query: string) {
    return `api/files${query ? `?search=${query}` : ''}`
}

export function fetchFiles(options = {}): responseInterface<ApiFile[], Error> {
    const { search } = useContext(Context)
    return useSWR(createEndpoint(search), fetch, { ...options })
}

export function mutateFiles(files: ApiFile[], search: string, revalidate: boolean = true) {
    mutate(createEndpoint(search), files, revalidate)
}
