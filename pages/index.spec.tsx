import React from 'react'
import { create, act } from 'react-test-renderer'
import IndexPage from '.'

it('renders correctly', async () => {
    let tree
    
    act(() => {
        tree = create(<IndexPage />)
    })

    expect(tree.toJSON()).toMatchSnapshot()
})
