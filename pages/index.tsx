import { useState } from 'react'

import PageContainer from 'components/PageContainer'
import DisableScreen from 'components/DisableScreen'

import Header from './components/Header'
import FileList from './components/FileList'

import Context, { ContextType } from './context'

export default function IndexPage() {
    const [search, setSearch] = useState('')
    const [error, setError] = useState(null)

    const value: ContextType = {
        search,
        setSearch
    }

    return (
        <Context.Provider value={value}>
            <>
                {error && (
                    <DisableScreen
                        message={error}
                        onClose={() => setError(null)}
                    />
                )}
                <PageContainer
                    renderHeader={Header}
                    renderFileList={FileList}
                />
            </>
        </Context.Provider>
    )
}
