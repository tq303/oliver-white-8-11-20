import React from 'react'

export type ApiFile = {
    uuid: string,
    name: string,
    size: number
}

export type ContextType = {
    search: string,
    setSearch: (arg: string) => void,
}

const contextDefaults: ContextType = {
    search: '',
    setSearch: arg => {},
}

const Context = React.createContext<ContextType>(contextDefaults)

export default Context

