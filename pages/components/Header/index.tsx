import UploadButton from '../UploadButton'
import SearchInput from '../SearchInput'
import FileDetails from '../FileDetails'

import styles from './Header.module.scss'

export default function Header () {

    return (
        <div>
            <div className={styles.container}>
                <UploadButton />
                <SearchInput />
            </div>
            <FileDetails />
        </div>
    )
}