import { useState, useContext } from 'react'

import DisableScreen from 'components/DisableScreen'

import styles from './FileList.module.scss'

import Context from '../../context'
import { fetchFiles, mutateFiles } from '../../hooks'

import Card from 'components/Card'

export default function FileList () {
    const { search } = useContext(Context)
    const { data: files, isValidating } = fetchFiles()

    const [toDeleteUuid, setToDelete] = useState(null)
    const [error, setError] = useState(null)

    async function handleDelete() {
        if (!toDeleteUuid) throw 'toDeleteUuid not set'

        try {
            const res = await fetch(`api/files/${toDeleteUuid}`, {
                method: 'DELETE'
            })

            if (!res.ok) {
                const error = await res.text()
                throw error
            }

            mutateFiles([], search)
        } catch (e) {
            setError(e)
        }
        
        setToDelete(null)
    }

    return (
        <div className={styles.container}>
            {isValidating && !toDeleteUuid && <DisableScreen message="Loading..." />}
            {isValidating && toDeleteUuid && <DisableScreen message="Updating..." />}
            {error && (
                <DisableScreen
                    message={error}
                    onClose={() => setError(null)}
                />
            )}
            {toDeleteUuid && (
                <DisableScreen
                    message={() => <span>You are about to delete <strong>{toDeleteUuid}</strong>. Do you wish to continue?</span>}
                    textClose="NO"
                    onClose={() => setToDelete(null)}
                    textAccept="YES"
                    onAccept={handleDelete}
                />
            )}
            {files && files.map(file => (
                <Card
                    key={file.uuid}
                    uuid={file.uuid}
                    name={file.name}
                    size={file.size}
                    onDelete={setToDelete}
                />
            ))}
        </div>
    )
}
