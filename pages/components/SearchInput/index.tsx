import { useContext } from 'react'

import Context from '../../context'

import styles from './SearchInput.module.scss'

export default function SearchInput ({ ...props }) {
    const { setSearch } = useContext(Context)

    function debounceSearch(wait = 200) {
        let timeout

        return e => {
            e.persist()

            clearTimeout(timeout)
            timeout = setTimeout(() => {
                setSearch(e.target.value)
            }, wait)
        }
    }

    return (
        <input
            { ...props }
            className={styles.input}
            placeholder="Search documents..."
            onChange={debounceSearch()}
            type="text"
        />
    )
}
