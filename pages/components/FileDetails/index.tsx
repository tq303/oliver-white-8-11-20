import { useContext } from 'react'

import styles from './FileDetails.module.scss'

import Context from '../../context'
import { fetchFiles } from '../../hooks'

export default function FileDetails () {
    const { search } = useContext(Context)
    const { data: files } = fetchFiles(search)

    const calculateFileSize = () => files ? files.reduce((acc, file) => acc + file.size, 0) : 0

    return (
        <div className={styles.container}>
            <h3 className={styles.count}>{files ? files.length : 0} documents</h3>
            <p className={styles.size}>Total size: {calculateFileSize()}kb</p>
        </div>
    )
}