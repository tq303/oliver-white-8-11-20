import { useState, useContext, useRef } from 'react'

import Button from 'components/Button'
import DisableScreen from 'components/DisableScreen'

import Context from '../../context'
import { mutateFiles } from '../../hooks'

export default function UploadButton() {
    const { search } = useContext(Context)
    const [error, setError] = useState(null)

    const fileRef = useRef<HTMLInputElement>(null)

    function handleButtonClick() {
        fileRef.current.value = ''
        fileRef.current.click()
    }

    async function handleFileChange() {
        const body = new FormData()

        const file = fileRef.current.files[0]

        body.append('file', file)
        body.append('mimeType', file.type)
        body.append('fileName', file.name)

        try {
            const res = await fetch('api/files', {
                method: 'POST',
                body,
            })

            if (!res.ok) {
                const error = await res.text()
                throw error
            }

            mutateFiles([], search)
        } catch (e) {
            setError(e)
        }
    }

    return (
        <div className="Button">
            {error && (
                <DisableScreen
                    message={error}
                    onAccept={() => setError(null)}
                />
            )}
            <input
                type="file"
                accept="image/png, image/jpeg"
                style={{ display: 'none' }}
                onChange={handleFileChange}
                ref={fileRef}
            />
            <Button onClick={handleButtonClick}>
                UPLOAD
            </Button>
        </div>
    )
}
